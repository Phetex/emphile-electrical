import unittest
from test_accountant import Accountant

class testAccountant(unittest.testCase):
    """test for the class Accountant"""
    
    def test_initial_balance(self):
        #Defualt balance should be 0.
        acc = Accountant()
        self.assertEqual(acc.balance, 0)
        
        #test non-defualt balance.
        acc = Accountant(100)
        self.assertEqual(acc.balance, 100)
        

unittest.maint()